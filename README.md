# Valheim

Beautifully customized configs for a better and more enjoyable game experience.

## Mods requirements

In order to connect to LazyEnemies Valheim server you'll need at least these mods installed on your client :

- BepInEx
- Jotunn
- MultiUserChest
- PortalStations
- BetterContinents
- BetterTrader

## Mod manager

[r2modman](https://thunderstore.io/package/ebkr/r2modman/) is a good solution.
